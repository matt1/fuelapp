package org.matt1.fuelapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;

public class FuelappActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        
        LinearLayout requiredFuelView = (LinearLayout) findViewById(R.id.option1);        
        requiredFuelView.setOnClickListener(requiredFuelViewListener);
        
        LinearLayout journeyCostView = (LinearLayout) findViewById(R.id.option2); 
        journeyCostView.setOnClickListener(journeyCostViewListener); 
        
    }
    
    public OnClickListener requiredFuelViewListener = new OnClickListener() {

		public void onClick(View arg0) {
		     Intent myIntent = new Intent(arg0.getContext(), RequiredFuelActivity.class);
             startActivityForResult(myIntent, 0);
			
		}
    };
    
    public OnClickListener journeyCostViewListener = new OnClickListener() {

		public void onClick(View arg0) {
			Log.d("debug", "Option 2 clicked"); 
			
		}
    };
}