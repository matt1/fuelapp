package org.matt1.fuelapp.utilities;

/**
 * Bad User Input exception
 * 
 * @author Matt
 *
 */
public class BadInputException extends Exception {

	private static final long serialVersionUID = 171607680607870118L;

	/**
	 * Name of parameter at fault 
	 * @author Matt
	 *
	 */
	public enum ParameterName {
		MPG,
		DISTANCE,
		PRICE,
		UNITS
	}
	
	/** Field that had an issue */
	private ParameterName mBadField = null;
	
	/**
	 * New bad input exception
	 * 
	 * @param pBadField
	 */
	public BadInputException(ParameterName pBadField) {
		super();
		mBadField = pBadField;
	}
	
	/**
	 * Return the field name that had the bad input.
	 * 
	 * @return
	 */
	public ParameterName getField() {
		return mBadField;
	}
	
}
