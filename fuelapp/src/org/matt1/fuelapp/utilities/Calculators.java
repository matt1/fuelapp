package org.matt1.fuelapp.utilities;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.matt1.fuelapp.utilities.BadInputException.ParameterName;


/**
 * Calculators for fuel consumption etc
 * 
 * @author Matt
 *
 */
public class Calculators {

	/** Miles per KM */
	private static final double MILES_IN_KM = 0.621371192;
	
	/** KM per mile */
	private static final double KM_IN_MILES = 1.609347219;
	
	/** Liters per gallon */
	private static final double LITRES_IN_GALLON = 4.54609188;
	
	/** Gallons per litre */
	private static final double GALLONS_IN_LITRE = 0.219969157;
	
	/** Decimal format for rounding */
	private static DecimalFormat mDecimalFormat = new DecimalFormat("#.##");
	
	/**
	 * Rounds all figures to something sensible, e.g. 2 decimal places
	 * @return
	 */
	public static final double round(double pInput) {
		BigDecimal input = new BigDecimal(Double.toString(pInput));
		input = input.setScale(2, BigDecimal.ROUND_HALF_UP);
		return input.doubleValue();
	}
	
	/**
	 * Converts a decimal to a rounded value for display
	 * 
	 * @param pAmount
	 * @return
	 */
	public static final String toString(double pAmount) throws BadInputException {
		return mDecimalFormat.format(pAmount);
	}
	
	
	/**
	 * Miles in KM
	 * 
	 * @param pKm
	 * @return
	 */
	public static final double kmToMiles(double pKm) throws BadInputException {
		if (pKm <= 0 || pKm > 1000000) {
			throw new BadInputException(ParameterName.DISTANCE);
		}
		return round(pKm * MILES_IN_KM);
	} 
	
	/**
	 * KM in miles
	 * 
	 * @param pMiles
	 * @return
	 */
	public static final double milesToKm(double pMiles) throws BadInputException {
		
		if (pMiles <= 0 || pMiles > 1000000) {
			throw new BadInputException(ParameterName.DISTANCE);
		}
		
		return round(pMiles * KM_IN_MILES);
	}
	
	/**
	 * Calculates the amount of fuel needed (gallons) to go a given distance
	 * 
	 * @param pMiles
	 * @param pMpg
	 * @return
	 */
	public static final double requiredFuel(double pMiles, double pMpg) throws BadInputException   {
		
		if (pMiles <= 0|| pMiles > 1000000 ) {
			throw new BadInputException(ParameterName.DISTANCE);
		} else if (pMpg <=0) {
			throw new BadInputException(ParameterName.MPG);
		}
		
		return round(pMiles/pMpg);
	}
	
	/**
	 * Calculates the cost of a journey based on distance, MPG and price
	 *  
	 * @param distance
	 * @param pMpg
	 * @param pPricePerLitre
	 * @return
	 */
	public static final double journeyCost(double pMiles, double pMpg, double pPricePerLitre) throws BadInputException {
		if (pMiles <= 0 || pMiles > 1000000) {
			throw new BadInputException(ParameterName.DISTANCE);
		} else if (pMpg <= 0 || pMpg > 250) {
			throw new BadInputException(ParameterName.MPG);
		} else if (pPricePerLitre <= 0 || pPricePerLitre > 1000) {
			throw new BadInputException(ParameterName.PRICE);
		}
		
		return round(gallonsToLitres(pMiles/pMpg) * pPricePerLitre);
	}
	
	/**
	 * Convert litres to gallon
	 * 
	 * @param pLitres
	 * @return
	 */
	public static final double litresToGallons(double pLitres) throws BadInputException {
		if (pLitres <=0) {
			throw new BadInputException(ParameterName.UNITS);
		}
		return round(pLitres * GALLONS_IN_LITRE);
	}
	

	/**
	 * Converts gallons to litre
	 * 
	 * @param pLitres
	 * @return
	 */
	public static final double gallonsToLitres(double pGallons) throws BadInputException {
		if (pGallons <=0) {
			throw new BadInputException(ParameterName.UNITS);
		}
		return round(pGallons * LITRES_IN_GALLON);
	}
	
	
}
