package org.matt1.fuelapp.test.utilities;

import junit.framework.Assert;

import org.junit.Test;
import org.matt1.fuelapp.utilities.BadInputException;
import org.matt1.fuelapp.utilities.BadInputException.ParameterName;
import org.matt1.fuelapp.utilities.Calculators;



public class CalculatorsTest {

	@Test
	public void testKmToMiles() throws BadInputException {
	
		Assert.assertEquals(1.0, Calculators.kmToMiles(1.609347219));
		Assert.assertEquals(0.62, Calculators.kmToMiles(1.0));
		Assert.assertEquals(11.76, Calculators.kmToMiles(18.927));
		
		try {
			Calculators.kmToMiles(0);
			Assert.fail();
		} catch (BadInputException e) {
			 Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.kmToMiles(-1);
			Assert.fail();
		} catch (BadInputException e) {
			 Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.kmToMiles(1000000.1);
			Assert.fail();
		} catch (BadInputException e) {
			 Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
	
		
	}
	
	@Test
	public void testMilesToKm() throws BadInputException {
		
		Assert.assertEquals(1.0, Calculators.milesToKm(0.621371192));
		Assert.assertEquals(44.15, Calculators.milesToKm(27.432));
		
		try {
			Calculators.milesToKm(0);
			Assert.fail();
		} catch (BadInputException e) {
			 Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.milesToKm(-1);
			Assert.fail();
		} catch (BadInputException e) {
			 Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.milesToKm(1000000.1);
			Assert.fail();
		} catch (BadInputException e) {
			 Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
	}
	
	@Test
	public void testRequiredFuel() throws BadInputException {
		Assert.assertEquals(1.0, Calculators.requiredFuel(35, 35.0));
		Assert.assertEquals(10.00, Calculators.requiredFuel(350, 35.0));
		
		try {
			Calculators.requiredFuel(-1, 30);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.requiredFuel(0, 30);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.requiredFuel(1000000.1, 30);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.requiredFuel(10, -1);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.MPG, e.getField());
		}
		
		try {
			Calculators.requiredFuel(10, 0);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.MPG, e.getField());
		}
		
	}
	
	@Test
	public void testLitresToGallon() throws BadInputException {
		
		Assert.assertEquals(0.22, Calculators.litresToGallons(1.0));
		Assert.assertEquals(3.74, Calculators.litresToGallons(17.0));
		
		try {
			Calculators.litresToGallons(-1);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.UNITS, e.getField());
		}
		
		try {
			Calculators.litresToGallons(0);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.UNITS, e.getField());
		}
	}
	
	@Test
	public void testGallonsToLitres() throws BadInputException {
		
		Assert.assertEquals(4.55, Calculators.gallonsToLitres(1.0));
		Assert.assertEquals(26.82, Calculators.gallonsToLitres(5.9));
		
		try {
			Calculators.litresToGallons(-1);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.UNITS, e.getField());
		}
		
		try {
			Calculators.litresToGallons(0);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.UNITS, e.getField());
		}
	}
	
	
	
	@Test
	public void testJourneyCost() throws BadInputException {
		// miles, mpg, price
		
		Assert.assertEquals(909.00, Calculators.journeyCost(100, 50.0, 100.0));
		Assert.assertEquals(4777.15, Calculators.journeyCost(278, 37.8, 142.9));
		
	
		try {
			Calculators.journeyCost(-1, 30, 100);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}

		try {
			Calculators.journeyCost(0, 30, 100);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.journeyCost(1000000.1, 30, 100);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.DISTANCE, e.getField());
		}
		
		try {
			Calculators.journeyCost(1, 30, -1);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.PRICE, e.getField());
		}

		try {
			Calculators.journeyCost(1, 30, 0);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.PRICE, e.getField());
		}
		
		try {
			Calculators.journeyCost(1, 30, 1000.1);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.PRICE, e.getField());
		}
		
		try {
			Calculators.journeyCost(1, 0, 100);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.MPG, e.getField());
		}
		
		try {
			Calculators.journeyCost(1, -1, 100);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.MPG, e.getField());
		}
		
		try {
			Calculators.journeyCost(1, 250.1, 100);
			Assert.fail();
		} catch (BadInputException e) {
			Assert.assertEquals(ParameterName.MPG, e.getField());
		}
		
	}
	
}
